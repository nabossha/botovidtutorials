<?php
if(isset($_GET["chapter"])) {
	$kapitelfilm = $_GET["chapter"];
} else {
	die("Kein Kapitel gesetzt!");	
}

?>
<html>
<head>
	<title>Typo3 Video-Handbuch</title>
    <style type="text/css">
	html, body {
		width: 100%;
		overflow: hidden;
	}
	body {
		margin: 0;
		padding: 0;
		border: 0;
		background-color: #3C3C3C;
		
		width: 100%;
	}
	</style>
</head>
<body>
<div id="jp_container_1" class="jp-video jp-video-full">
    <iframe src="//player.vimeo.com/video/<? echo($kapitelfilm); ?>" width="500" height="281" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
</div>
</body>
</html>
