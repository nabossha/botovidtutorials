/**
 * Created by nabossha on 18.05.2014.
 */
function VideoFenster(videoID) {
    var sName    = Math.floor((Math.random() * 1000) + 1);
    var videoW = 740;
    var videoH = 410;
    //alert("VideoFenster" + videoID);
    var htmlcode = '<div id="contentwrap"><iframe src="//player.vimeo.com/video/' + videoID + '" width="'+videoW+'" height="'+videoH+'" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe></div>';


    var sOptions = 'toolbar=no,scrollbars=yes,location=no,statusbar=no,menubar=no,resizable=1,width=' + (videoW + 50).toString() + ',height=' + (videoH + 50).toString();
    var generator=window.open('',sName,sOptions);

    generator.document.write('<html><head><style type="text/css">html, body {margin: 0; padding: 0; width: 100%;} body {background-color: #F5F5F5;} #contentwrap, IFRAME { margin: auto; width: 100%; height: 100%;}</style></style><title>Video-Tutorial</title>');
    generator.document.write(htmlcode);
    generator.document.write('</body></html>');
    generator.document.close();
}