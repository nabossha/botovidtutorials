<?php

/***************************************************************
 * Extension Manager/Repository config file for ext "botovidtutorials".
 *
 * Auto generated 13-12-2012 17:35
 *
 * Manual updates:
 * Only the data in the array - everything else is removed by next
 * writing. "version" and "dependencies" must not be touched!
 ***************************************************************/

$EM_CONF[$_EXTKEY] = array(
	'title' => 'Backend Video-Tutorials for TYPO3-editors',
	'description' => '',
	'category' => 'doc',
	'author' => 'Nando Bosshart',
	'author_email' => 'typo3@bosshartong.ch',
    'author_company' => 'BossharTong GmbH',
	'priority' => '',
	'module' => 'mod1',
	'state' => 'stable',
	'internal' => '',
	'uploadfolder' => 0,
	'createDirs' => '',
	'modify_tables' => '',
	'clearCacheOnLoad' => 1,
	'lockType' => '',
	'version' => '2.0.3',
    'constraints' => array(
        'depends' => array(
            'extbase' => '8.7'
        ),
        'conflicts' => array(
        ),
        'suggests' => array(
        ),
    ),
);

?>