module.tx_botovidtutorials {
    view {
        templateRootPaths {
            0 = EXT:botovidtutorials/Resources/Private/Templates/
        }
        partialRootPaths {
            0 = EXT:botovidtutorials/Resources/Private/Partials/
        }
        layoutRootPaths {
            0 = EXT:botovidtutorials/Resources/Private/Layouts/
        }
    }
}
