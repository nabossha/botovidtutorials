<?php
if (!defined('TYPO3_MODE')) {
    die ('Access denied.');
}

if (TYPO3_MODE === 'BE') {
    /**
     * Registers "Videotutorials" backend module
     */
    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerModule(
        'Bosshartong.' . $_EXTKEY,
        'help',
        'botovidtutorials',
        '',
        array(
            'Botovidtutorials' => 'index, debuginfo, ticket'
        ),
        array(
            'access' => 'user,group',
            'icon'   => 'EXT:' . $_EXTKEY . '/Resources/Public/Icons/module.png',
            'labels' => 'LLL:EXT:' . $_EXTKEY . '/Resources/Private/Language/locallang_vidtutorials.xlf',
        )
    );
}

